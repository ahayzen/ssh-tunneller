<!--
SPDX-FileCopyrightText: 2019 Andrew Hayzen <ahayzen@gmail.com>

SPDX-License-Identifier: MPL-2.0
-->

A Python script which sets up a SSH remote forwarding session using JSON config, and then monitors that the port remains open - restarting the SSH session if necessary.

# Configuration

An example configuration of a JSON file might be the following.

```json
{
    "devhttpserver": {
        "local_host": "localhost",
        "local_port": 80,
        "remote_host": "example.com",
        "remote_port": 8000,
        "ssh_port": 22,
        "ssh_private_key": "/home/user/.ssh/id_rsa",
        "ssh_user": "user"
    }
}
```

This would setup remote forwarding so that accessing the port 8000 on `example.com` accesses port 80 on `localhost`, and it uses the ssh account `user` with a given private key.

# Running

To use this config, simply pass the path to the file or a folder of multiple files as an argument.

```bash
python3 main.py --config /path/to/folder
python3 main.py --config /path/to/file
```

You can also pass further parameters and if `python3-systemd` isn't installed redirect output to a log file.

```bash
python3 main.py --config /path/to/folder --interval 60 --timeout 10 &>> /tmp/forwarder.log
```

Also note that the script works with python2.