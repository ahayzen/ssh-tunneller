#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2019, 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

from argparse import ArgumentParser
from json import loads as json_loads
from logging import getLogger, DEBUG as logging_DEBUG, Formatter
from os import getcwd, listdir
from os.path import isdir, isfile, islink, join
from signal import signal, SIGTERM
import socket
from subprocess import PIPE, Popen
from time import sleep


try:
    from json.decoder import JSONDecodeError
except ImportError:
    # Python 2 compatiblity
    JSONDecodeError = ValueError


# Try to use systemd journal, otherwise fallback to std{err,out}
try:
    from systemd.journal import JournalHandler
    HANDLER = JournalHandler()
    HANDLER.setFormatter(Formatter())
except ImportError:
    from logging import StreamHandler
    HANDLER = StreamHandler()
    HANDLER.setFormatter(Formatter("%(asctime)s: %(message)s"))


class Logger(object):
    __logger = getLogger("ssh-forwarder")
    debug = __logger.debug
    info = __logger.info
    warning = __logger.warning
    error = __logger.error
    critical = __logger.critical

    @classmethod
    def init(cls):
        cls.__logger.addHandler(HANDLER)
        cls.__logger.setLevel(logging_DEBUG)


class Conn(object):
    def __init__(self, json, name=None, timeout=5):
        self.cached_remote_addr = None
        self.control_socket = "/dev/shm/ssh-forwarder-%s" % name
        self.name = name
        self.pid = None
        self.timeout = timeout

        # Read JSON data
        self.local_addr = json["local_host"]  # localhost
        self.local_port = json["local_port"]  # 80
        self.remote_host = json["remote_host"]
        self.remote_port = json["remote_port"]  # 8080
        self.ssh_port = json["ssh_port"]  # 22
        self.ssh_private_key = json["ssh_private_key"]  # ~/.ssh/id_rsa
        self.ssh_user = json["ssh_user"]

        # Cache the initial remote address, so that if
        # DNS ever fails later on, we don't crash or have invalid ip
        self.cached_remote_addr = self.remote_addr
        # If we can't start the service with a valid ip then stop
        if self.cached_remote_addr is None:
            raise SystemExit

    def __repr__(self):
        name = "" if self.name is None else " (%s)" % self.name
        return "%s:%s -> %s:%s%s" % (self.remote_addr, self.remote_port,
                                     self.local_addr, self.local_port,
                                     name)

    @property
    def bind_address(self):
        return "%s:%s:%s" % (self.remote_port, self.local_addr,
                             self.local_port)

    def is_open(self):
        return is_port_open(self, self.timeout)

    @property
    def remote_addr(self):
        return ipaddr_for_hostname(self.remote_host) or self.cached_remote_addr

    def run(self):
        self.pid = ssh_forward(self)

    def stop(self):
        ssh_control_stop(self)

        if self.pid is not None:
            self.pid.kill()
            self.pid = None


class Store(object):
    def __init__(self, timeout):
        self.conns = {}
        self.timeout = timeout

    def add(self, conn):
        if conn.name not in self.conns:
            self.conns[conn.name] = conn
        else:
            Logger.error("Duplicate connection name: %s" % conn.name)
            raise SystemExit

    def add_from_file(self, filepath):
        # Load json from file
        try:
            json = read_conf_file(filepath)
        except JSONDecodeError as e:
            Logger.error("Could not load file: %s" % filepath)
            Logger.error("Exception: %s" % e)
            exit(1)

        # Load each subsection of the file
        for name in json:
            self.add(Conn(json=json[name], name=name, timeout=self.timeout))

    def close(self):
        for conn in self.conns.values():
            conn.stop()

    def monitor(self):
        for conn in self.conns.values():
            if not conn.is_open():
                conn.stop()
                conn.run()

    def monitor_wait(self, wait=30):
        while True:
            # Sleep first to leave time for the first connection to init
            sleep(wait)

            self.monitor()

    def run(self):
        for conn in self.conns.values():
            conn.run()


def ipaddr_for_hostname(hostname):
    try:
        return socket.gethostbyname(hostname)
    except socket.gaierror:
        Logger.error("Could not resolve hostname: %s" % hostname)
        return None


def is_port_open(conn, timeout):
    sock = socket.socket()
    sock.settimeout(timeout)
    err = sock.connect_ex((conn.remote_addr, conn.remote_port))
    sock.close()

    return err == 0


def read_conf_file(filepath):
    Logger.info("<-- Loading config file: %s" % filepath)

    with open(filepath, "rb") as f:
        return json_loads(f.read())


def run_command(cmd, stdout=PIPE, **kwargs):
    Logger.debug("   $ '%s'" % " ".join(cmd))
    return Popen(cmd, stdout=stdout, **kwargs)


def ssh_forward(conn):
    Logger.info("--> Forwarding connection: %s" % conn)

    return run_command([
        "/bin/ssh",
        "-nNTM",
        "-o", "ServerAliveInterval=60",
        "-o", "ServerAliveCountMax=3",
        "-p", str(conn.ssh_port),
        "-S", conn.control_socket,
        "-i", conn.ssh_private_key,
        "-R", conn.bind_address,
        "%s@%s" % (conn.ssh_user, conn.remote_host)])


def ssh_control_stop(conn):
    Logger.info("--X: Stopping connection: %s" % conn)

    run_command([
        "/bin/ssh",
        "-S", conn.control_socket,
        "-O", "stop",
        "%s@%s" % (conn.ssh_user, conn.remote_host)
    ]).wait()


if __name__ == "__main__":
    # Setup command line arguments
    parser = ArgumentParser()
    parser.add_argument("-c", "--config", dest="config",
                        help="directory or file to scan for json CONFIG",
                        default=getcwd())
    parser.add_argument("-i", "--interval", dest="interval",
                        help="monitor time INTERVAL", default=60, type=int)
    parser.add_argument("-t", "--timeout", dest="timeout",
                        help="TIMEOUT when checking sockets", default=5,
                        type=int)
    args = parser.parse_args()

    # Initialise classes
    Logger.init()
    store = Store(args.timeout)

    # Print current arguments
    Logger.info("Config: path='%s'" % args.config)
    Logger.info("Config: interval=%s" % args.interval)
    Logger.info("Config: timeout=%s" % args.timeout)

    # Scan given config for files
    if isdir(args.config):
        for f in [join(args.config, f) for f in listdir(args.config)
                  if isfile(join(args.config, f))]:
            store.add_from_file(f)
    elif isfile(args.config) or islink(args.config):
        store.add_from_file(args.config)
    else:
        Logger.error("Config path is not a directory, file, or link: %s"
                     % args.config)
        exit(1)

    # Handle SIGTERM gracefully
    def handle_exit(sig, frame):
        raise SystemExit

    signal(SIGTERM, handle_exit)

    # Monitor the connections, restarting if required
    try:
        store.run()  # Start the loaded connections

        store.monitor_wait(args.interval)
    except (KeyboardInterrupt, SystemExit):
        Logger.info("Exit called, closing connections")

    store.close()
